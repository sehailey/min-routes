# Minimal Working Example for Routes

This example emulates the project structure for the weatcb-react repository
with a correct implementation of passing functions to the components and
passing state through Router Links.
