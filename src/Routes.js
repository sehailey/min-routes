import React from 'react'
import { Tasks } from './components'
import { Route } from 'react-router-dom'

const Routes = props => {
  return (
    <div>
      <Route
        path="/tasks"
        render={routeProps => <Tasks {...routeProps} {...props} />}
      />
    </div>
  )
}

export default Routes
