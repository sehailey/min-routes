import React from 'react'
import { Link } from 'react-router-dom'

class TaskDetail extends React.Component {
  constructor() {
    super()
    this.state = {}
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount() {
    this.setState({ ...this.props.location.state })
  }
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmit(e) {
    e.preventDefault()
    this.props.updateTask(this.state)
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <span>Editing Task {this.state.id}: </span>
        <input
          name="name"
          value={this.state.name}
          onChange={this.handleChange}
        />
        <button onSubmit={this.handleSubmit}>submit</button>
        <Link to={'/tasks'}>
          <button>Home</button>
        </Link>
      </form>
    )
  }
}

export default TaskDetail
