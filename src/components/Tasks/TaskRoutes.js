import React from 'react'
import { Route, Switch } from 'react-router-dom'

import TaskView from './TaskView'
import TaskDetail from './TaskDetail'
import TaskCreate from './TaskCreate'

// the route to /tasks/create does not need routeProps
// because it does not need to be passed data

// the route to /tasks/id needs routeProps because
// it is passed data from the TaskView Link
const TaskRoutes = props => {
  return (
    <Switch>
      <Route
        path="/tasks/create"
        render={() => <TaskCreate createTask={props.createTask} />}
      />
      <Route
        path={`/tasks/:id`}
        render={routeProps => (
          <TaskDetail updateTask={props.updateTask} {...routeProps} />
        )}
      />
      <Route path="/tasks" render={() => <TaskView {...props} />} />
    </Switch>
  )
}

export default TaskRoutes
