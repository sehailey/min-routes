import React from 'react'
import TaskRoutes from './TaskRoutes'
const initialState = { loading: true, users: [], tasks: [] }
const tasks = [{ id: 0, name: 'task1' }, { id: 1, name: 'task2' }]

class Tasks extends React.Component {
  constructor() {
    super()
    this.state = initialState
    this.createTask = this.createTask.bind(this)
    this.updateTask = this.updateTask.bind(this)
  }

  async fetchTasks() {
    try {
      const data = await tasks
      return data
    } catch (e) {
      console.log(e)
    }
  }

  async fetchData() {
    const users = await this.props.users
    const tasks = await this.fetchTasks()
    await this.setState({ users, tasks, loading: false })
  }
  componentWillMount() {
    this.fetchData()
  }

  createTask(task) {
    // this is a fake id. the real id would be provided by the
    // API when the post request completes successfully
    task.id = this.state.tasks.length
    try {
      const tasks = this.state.tasks.concat(task)
      this.setState({ tasks })
      this.props.history.push('/tasks')
    } catch (e) {
      console.log(e)
      alert('Something went wrong. Please try again.')
    }
  }
  updateTask(task) {
    try {
      const tasks = this.state.tasks.map(t => (t.id === task.id ? task : t))
      this.setState({ tasks })
      this.props.history.push('/tasks')
    } catch (e) {
      console.log(e)
      alert('Something went wrong. Please try again.')
    }
  }
  renderLoading() {
    return <div>loading...</div>
  }

  renderError() {
    return <div>something went wrong.</div>
  }

  renderTasks() {
    return (
      <TaskRoutes
        createTask={this.createTask}
        updateTask={this.updateTask}
        {...this.state}
      />
    )
  }

  render() {
    if (this.state.loading) return this.renderLoading()
    if (this.state.error) return this.renderError()
    return this.renderTasks()
  }
}

export default Tasks
