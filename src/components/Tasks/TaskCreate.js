import React from 'react'

class TaskCreate extends React.Component {
  constructor() {
    super()
    this.state = { name: '' }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmit(e) {
    e.preventDefault()
    this.props.createTask(this.state)
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        Create a Task
        <input
          name="name"
          value={this.state.name}
          onChange={this.handleChange}
        />
        <button onSubmit={this.handleSubmit}>submit</button>
      </form>
    )
  }
}

export default TaskCreate
