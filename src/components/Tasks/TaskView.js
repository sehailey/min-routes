import React from 'react'
import { Link } from 'react-router-dom'

const TaskView = props => {
  const user = props.users[0]
  return (
    <div>
      <div>Hello, {user.firstName}!</div>

      {props.tasks.map(t => (
        <div key={t.id}>
          <Link
            to={{
              pathname: `/tasks/${t.id}`,
              state: { ...t },
            }}
          >
            {t.name}
          </Link>
        </div>
      ))}
      <Link to={'/tasks/create'}>
        <button>New Task</button>
      </Link>
    </div>
  )
}

export default TaskView
