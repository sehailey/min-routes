import React from 'react'
import Routes from './Routes'
const initialState = { loading: true, users: [] }
const users = [{ id: 0, firstName: 'Scott' }]

class App extends React.Component {
  constructor() {
    super()
    this.state = initialState
  }

  async fetchUsers() {
    try {
      const data = await users
      return data
    } catch (e) {}
  }

  async fetchData() {
    const users = await this.fetchUsers()
    this.setState({ users, loading: false })
  }

  componentWillMount() {
    this.fetchData()
  }
  renderLoading() {
    return <div>loading...</div>
  }

  renderError() {
    return <div>something went wrong.</div>
  }

  renderApp() {
    return <Routes users={this.state.users} />
  }

  render() {
    if (this.state.loading) return this.renderLoading()
    if (this.state.error) return this.renderError()
    return this.renderApp()
  }
}

export default App
